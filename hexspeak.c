#include <string.h>
#include <stdio.h>

int hexspeak(char *str, int len);

int hexspeak(char *str, int len) {
	int i;
	int p = len;
	for (i = 0; i < len; i++) {
		switch(*(str + i)) {
			case 'o':
				printf("%c", '0');
				break;
			case 'O':
				printf("%c", '0');
				break;
			case 'l':
				printf("%c", '1');
				break;
			case 'L':
				printf("%c", '1');
				break;
			case 's':
				printf("%c", '5');
				break;
			case 'S':
				printf("%c", '5');
				break;
			case 't':
				printf("%c", '7');
				break;
			case 'T':
				printf("%c", '7');
				break;
			case 'r':
				printf("%c", '1');
				printf("%c", '2');
				break;
			case 'R':
				printf("%c", '1');
				printf("%c", '2');
				break;
			case 'G':
				printf("%c", '6');
				break;
			case 'g':
				printf("%c", '9');
				break;
			default:
				printf("%c", *(str + i));
				p--;
				break;
		}
	}
	return p;
}