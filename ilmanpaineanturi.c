#include <inttypes.h>

uint32_t ilmanpaine(uint8_t xlsb, uint8_t lsb, uint8_t msb);

uint32_t ilmanpaine(uint8_t xlsb, uint8_t lsb, uint8_t msb) {
	uint32_t valitulos = msb;
	valitulos = msb << 8;
	valitulos = valitulos ^ lsb;
	valitulos = valitulos << 4;
	return (valitulos ^ (xlsb & 0xF0) >> 4);
}