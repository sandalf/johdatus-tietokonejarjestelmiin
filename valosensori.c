#include <inttypes.h>
#include <math.h>
#include <stdio.h>

float valoisuus(uint16_t rekisteri);

float valoisuus(uint16_t rekisteri) {
	int maski1 = 0x0FFF;
	int maski2 = 0xF000;
	return 0.01 * pow(2, (rekisteri & maski2) >> 12) * (rekisteri & maski1);
}