#include <math.h>

struct piste {
   int x;
   int y;
};

struct laatikko {
   struct piste max;
   struct piste min;
   struct piste pisteet[10];
};

void etsi_maxmin(struct laatikko *box);

void etsi_maxmin(struct laatikko *box) {
	int min = sqrt(pow(box->pisteet[0].x, 2) + pow(box->pisteet[0].y, 2));
	int max = min;
	int i;
	int p;
	for (i = 1; i < 10; i++) {
		p = sqrt(pow(box->pisteet[i].x, 2) +pow(box->pisteet[i].y, 2));
		if (p < min) {
			box->min = box->pisteet[i];
			min = p;
		} else if (p > max) {
			box->max = box->pisteet[i];
			max = p;
		}
	}
}