double laske_liike_energia(double nopeus, double massa);

double laske_liike_energia(double nopeus, double massa) {
	return ((1.0/2.0) * massa * nopeus * nopeus);
}