#include <inttypes.h>

int64_t laske_kertoma(int8_t n);

int64_t laske_kertoma(int8_t n) {
	if (n > 20) {
		return -1;
	} else {
		int64_t kertoma = 1;
		int i;
		for (i = 2; i <= n; i++) {
			kertoma *= i;
		}
		return kertoma;
	}
}