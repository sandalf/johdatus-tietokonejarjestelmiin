#include <inttypes.h>

float lampotila(uint16_t rekisteri, float kerroin);

float lampotila(uint16_t rekisteri, float kerroin) {
	uint16_t maski = 0xFFFC;
	return ((rekisteri & maski) >> 2) * kerroin;
}