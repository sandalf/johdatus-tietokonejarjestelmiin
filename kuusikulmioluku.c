#include <inttypes.h>

uint8_t kuusikulmio(uint32_t luku);

uint8_t kuusikulmio(uint32_t luku) {
	int i = 1;
	while (1) {
		if ((i * (2 * i - 1)) == luku) {
			return 1;
		} else if ((i * (2 * i - 1)) > luku) {
			return 0;
		} else {
			i++;
		}
	}
}