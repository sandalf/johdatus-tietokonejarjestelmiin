#include <inttypes.h>
#include <string.h>

int8_t parse(char *str, char *sep, char *arg);

int8_t parse(char *str, char *sep, char *arg) {
	char *token;
	int i = 0;
	token = strtok(str, sep);
	while(token != NULL) {
		if (!strncmp(token, arg, strlen(arg))) {
			return i;
		} else {
			token = strtok(NULL, sep);
			i++;
		}
	}
	return -1;
}