/*
 *  Modified by:
 *  Saku Salo
 *  Iina Lehtihalmes
 */

#include <stdio.h>
#include <string.h>
#include <animation.h>
#include <math.h>

/* XDCtools files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/drivers/PIN.h>
#include <ti/drivers/pin/PINCC26XX.h>
#include <ti/drivers/I2C.h>
#include <ti/drivers/i2c/I2CCC26XX.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/power/PowerCC26XX.h>
#include <ti/mw/display/Display.h>
#include <ti/mw/display/DisplayExt.h>

/* Board Header files */
#include "Board.h"

#include "wireless/comm_lib.h"
#include "sensors/bmp280.h"
#include "sensors/mpu9250.h"

//Task variables
#define STACKSIZE 2048
Char commTaskStack[STACKSIZE];
Char taskStack[STACKSIZE];
Char mainTaskStack[STACKSIZE];

//Display variable
Display_Handle hDisplay;
Display_Params displayParams;

//Device orientation variables
enum position {UNKNOWN=1, DISPLAY_IN, DISPLAY_OUT, DISPLAY_LEFT, DISPLAY_RIGHT, DISPLAY_UP, DISPLAY_DOWN};
enum position currentPosition = DISPLAY_UP;
enum position lastPosition = DISPLAY_UP;
//Finite-State machine variables
enum state {ANIMATION=1, MENU, STATS, GESTURE, MSG_WAITING};
enum state myState = ANIMATION;
enum state returnState;
//Gesture detection variables
float baselineX = 0.0f;
float baselineY = 0.0f;
float baselineZ = 0.0f;
//Menu option selection variable
int menuOption = 0;
//Gesture stat variables
int gestureH5Count = 0;
int gestureL5Count = 0;
int gestureFBCount = 0;
int gestureFUCount = 0;
//Display instruction variables
int updateDisplay = 0;
int clearDisplay = 0;
//Acceleration and gyro variables
float ax, ay, az, gx, gy, gz;
//Message variables
uint16_t sender;
char message[16];
int messageWaiting = 0;
int showMessage = 0;

//Button pin variables
static PIN_Handle buttonHandle;
static PIN_State buttonState;

//LED pin variables
static PIN_Handle ledHandle;
static PIN_State ledState;

//Buzzer pin variables
static PIN_Handle buzzerHandle;
static PIN_State buzzerState;

// MPU pin variables
static PIN_Handle mpuHandle;
static PIN_State mpuState;

//Power button pin variables
static PIN_Handle powerHandle;
static PIN_State powerState;

//Button pin config
PIN_Config buttonConfig[] = {
   Board_BUTTON0  | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
   PIN_TERMINATE
};

//LED pin config
PIN_Config ledConfig[] = {
   Board_LED1 | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW | PIN_PUSHPULL | PIN_DRVSTR_MAX, 
   PIN_TERMINATE
};

//Buzzer pin config
PIN_Config buzzerConfig[] = {
    Board_BUZZER | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

//MPU pin config
static PIN_Config mpuConfig[] = {
    Board_MPU_POWER  | PIN_GPIO_OUTPUT_EN | PIN_GPIO_HIGH | PIN_PUSHPULL | PIN_DRVSTR_MAX,
    PIN_TERMINATE
};

//MPU i2c config
static const I2CCC26XX_I2CPinCfg i2cMPUCfg = {
    .pinSDA = Board_I2C0_SDA1,
    .pinSCL = Board_I2C0_SCL1
};

//Power buttoon pin configs
PIN_Config powerFConfig[] = {
   Board_BUTTON1 | PIN_INPUT_EN | PIN_PULLUP | PIN_IRQ_NEGEDGE,
   PIN_TERMINATE
};
PIN_Config powerNConfig[] = {
   Board_BUTTON1 | PIN_INPUT_EN | PIN_PULLUP | PINCC26XX_WAKEUP_NEGEDGE,
   PIN_TERMINATE
};

//MPU Sensor task
Void sensorFxn(UArg arg0, UArg arg1) {
	I2C_Handle i2cMPU; // INTERFACE FOR MPU9250 SENSOR
	I2C_Params i2cMPUParams;

    I2C_Params_init(&i2cMPUParams);
    i2cMPUParams.bitRate = I2C_400kHz;
    i2cMPUParams.custom = (uintptr_t)&i2cMPUCfg;

    // MPU OPEN I2C
    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
    if (i2cMPU == NULL) {
        System_abort("Error Initializing I2CMPU\n");
    }

    // MPU POWER ON
    PIN_setOutputValue(mpuHandle, Board_MPU_POWER, Board_MPU_POWER_ON);

    // WAIT 100MS FOR THE SENSOR TO POWER UP
	Task_sleep(100000 / Clock_tickPeriod);
    System_printf("MPU9250: Power ON\n");
    System_flush();

    // MPU9250 SETUP AND CALIBRATION
	System_printf("MPU9250: Setup and calibration...\n");
	System_flush();

	mpu9250_setup(&i2cMPU);

	System_printf("MPU9250: Setup and calibration OK\n");
	System_flush();

    // MPU CLOSE I2C
    I2C_close(i2cMPU);

    // LOOP FOREVER
	while (1) {
	    if (myState == GESTURE) {
	        
    	    // MPU OPEN I2C
    	    i2cMPU = I2C_open(Board_I2C, &i2cMPUParams);
    	    if (i2cMPU == NULL) {
    	        System_abort("Error Initializing I2CMPU\n");
    	    }
    
    	    // MPU ASK DATA
            // Accelerometer values: ax,ay,az
    	 	// Gyroscope values: gx,gy,gz
    		mpu9250_get_data(&i2cMPU, &ax, &ay, &az, &gx, &gy, &gz);
            
    	    // MPU CLOSE I2C
    	    I2C_close(i2cMPU);
    
    	    // WAIT 100MS
        	Task_sleep(100000 / Clock_tickPeriod);
	    } else {
	        Task_sleep(100000 / Clock_tickPeriod);
	    }
	}

	// MPU9250 POWER OFF 
	// Because of loop forever, code never goes here
    PIN_setOutputValue(mpuHandle,Board_MPU_POWER, Board_MPU_POWER_OFF);
}

//Communication Task
Void commTaskFxn(UArg arg0, UArg arg1) {
    //"Clear" possible old messages by setting messageWaiting to False
    messageWaiting = 0;

    //Radio to receive mode
    int32_t result = StartReceive6LoWPAN();
    if(result != true) {
       System_abort("Wireless receive start failed");
    }
   
    //While loop to receive messages
    while (true) {

        //If true, we have a message
        if (GetRXFlag()) {

            //Empty message buffer
            memset(message,0,16);
            //Read message to message buffer
            Receive6LoWPAN(&sender, message, 16);
            //Set messageWaiting to True
            messageWaiting = 1;
            //Check current state to make right preparations for displaying the message
            if (myState != GESTURE) {
                returnState = myState;
                if (myState == ANIMATION) {
                    stopAnimation();
                }
                myState = MSG_WAITING;
                //Set display instruction variables to True
                clearDisplay = 1;
                updateDisplay = 1;
            }
            //Set notification LED on and play notification sound
            PIN_setOutputValue(ledHandle, Board_LED1, 1);
            playNotificationSound();
      }
   }
}

//Function for power button interruption
Void buttonShutFxn(PIN_Handle handle, PIN_Id pinId) {
    //Check state
    switch (myState) {
        case ANIMATION:
            //Display off
            Display_clear(hDisplay);
            Display_close(hDisplay);
            Task_sleep(100000 / Clock_tickPeriod);
        
            //Put SensorTag to sleep mode
            PIN_close(powerHandle);
            PINCC26XX_setWakeup(powerNConfig);
            Power_shutdown(NULL,0);
            break;
        case MENU:
            //If-else structure for menu movement
            if (menuOption == 2) {
                myState = ANIMATION;
            } else if (menuOption == 0) {
                myState = STATS;
                //Set display instruction variables to True(in STATS mode 0 = True for updateDisplay)
                updateDisplay = 0;
                clearDisplay = 1;
            } else if (menuOption == 1) {
                myState = GESTURE;
                updateDisplay = 0;
                clearDisplay = 1;
            }
            break;
        case GESTURE:
            //Show message in GESTURE mode
            if (messageWaiting) {
                showMessage = abs(showMessage - 1);
                if (!showMessage) {
                    messageWaiting = 0;
                }
            }
            break;
        case MSG_WAITING:
            //Preparation for clearing the message from the screen and changing back to previous state
            myState = returnState;
            PIN_setOutputValue(ledHandle, Board_LED1, 0);
            if (returnState == MENU) {
                //Set display instruction variables to True
                clearDisplay = 1;
                updateDisplay = 1;
            } else if (returnState == STATS) {
                //Set display instruction variables to True(in STATS mode 0 = True for updateDisplay)
                clearDisplay = 1;
                updateDisplay = 0;
            }
    }
}

//Function for button interruption
Void buttonFxn(PIN_Handle handle, PIN_Id pinId) {
    //Check state
    switch (myState) {
        case ANIMATION:
            //Pause animation and open up the menu screen
            myState = MENU;
            stopAnimation();
            updateDisplay = 1;
            break;
        case STATS:
            //Open up the menu screen
            myState = MENU;
            //Set display instruction variables to True
            clearDisplay = 1;
            updateDisplay = 1;
            break;
        case GESTURE:
            //Open up the menu screen
            myState = MENU;
            //Set display instruction variables to True
            clearDisplay = 1;
            updateDisplay = 1;
        case MENU:
            //Change menu screen selection
            menuOption = (menuOption + 1) % 3;
            //Set display instruction variable to True
            updateDisplay = 1;
            break;
   }
}

//Function for drawing the menu screen
Void drawMenu(Display_Handle hDisplay) {
    //Clear display if needed
    if (clearDisplay) {
        clearDisplay = 0;
        Display_clear(hDisplay);
    }
    //Display menu options on screen
    char option1[20];
    char option2[20];
    char option3[20];
    //If-Else to check which option is selected
    if (menuOption == 0) {
        sprintf(option1, "%s", "> Stats");
    } else {
        sprintf(option1, "%s", "Stats");
    }
    if (menuOption == 1) {
        sprintf(option2, "%s", "> Gesture Mode");
    } else {
        sprintf(option2, "%s", "Gesture Mode");
    }
    if (menuOption == 2) {
        sprintf(option3, "%s", "> IDLE");
    } else {
        sprintf(option3, "%s", "IDLE");
    }
    //Print options to screen
    Display_print0(hDisplay, 1, 1, option1);
    Display_print0(hDisplay, 5, 1, option2);
    Display_print0(hDisplay, 9, 1, option3);
    //Draw dividers between menu options
    tContext *pContext = DisplayExt_getGrlibContext(hDisplay);
    GrLineDrawH(pContext, 0, 96, 0);
    GrLineDrawH(pContext, 0, 96, 20);
    GrLineDrawH(pContext, 0, 96, 32);
    GrLineDrawH(pContext, 0, 96, 52);
    GrLineDrawH(pContext, 0, 96, 64);
    GrLineDrawH(pContext, 0, 96, 84);
    GrFlush(pContext);
}

//Function for drawing the stats screen
Void drawStats(Display_Handle hDisplay) {
    //Clear display if needed
    if (clearDisplay) {
        clearDisplay = 0;
        Display_clear(hDisplay);
    }
    //Display stats on the screen
    char stat1[40];
    char stat2[40];
    char stat3[40];
    char stat4[40];
    sprintf(stat1, "%s%i", "High Fives: ", gestureH5Count);
    sprintf(stat2, "%s%i", "Low Fives: ", gestureL5Count);
    sprintf(stat3, "%s%i", "Fist Bumps: ", gestureFBCount);
    sprintf(stat4, "%s%i", "Fist Ups: ", gestureFUCount);
    //Print stats on screen
    Display_print0(hDisplay, 1, 1, stat1);
    Display_print0(hDisplay, 4, 1, stat2);
    Display_print0(hDisplay, 7, 1, stat3);
    Display_print0(hDisplay, 10, 1, stat4);
    //Draw dividers between stats
    tContext *pContext = DisplayExt_getGrlibContext(hDisplay);
    GrLineDrawH(pContext, 0, 96, 3);
    GrLineDrawH(pContext, 0, 96, 18);
    GrLineDrawH(pContext, 0, 96, 28);
    GrLineDrawH(pContext, 0, 96, 43);
    GrLineDrawH(pContext, 0, 96, 51);
    GrLineDrawH(pContext, 0, 96, 67);
    GrLineDrawH(pContext, 0, 96, 76);
    GrLineDrawH(pContext, 0, 96, 91);
    GrFlush(pContext);
}

//Function for checking and updating device orientation variable
Void updatePosition() {
    //If-Else to figure device orientation based on gravity, update current and last position if needed
    if (ax < 1.10f && ax > 0.90f && abs(ay) < 0.20f && abs(az) < 0.20f) {
        if (currentPosition != DISPLAY_IN) {
            lastPosition = currentPosition;
            currentPosition = DISPLAY_IN;
            playStandBySound();
        }
        //Save current acceleration values for gesture detection
        baselineX = ax;
        baselineY = ay;
        baselineZ = az;
    } else if(ax > -1.10f && ax < -0.90f && abs(ay) < 0.20f && abs(az) < 0.20f) {
        if (currentPosition != DISPLAY_OUT) {
            lastPosition = currentPosition;
            currentPosition = DISPLAY_OUT;
        }
    } else if (ay < 1.10f && ay > 0.90f && abs(ax) < 0.20f && abs(az) < 0.20f) {
        if (currentPosition != DISPLAY_LEFT) {
            lastPosition = currentPosition;
            currentPosition = DISPLAY_LEFT;
        }
    } else if(ay > -1.10f && ay < -0.90f && abs(ax) < 0.20f && abs(az) < 0.20f) {
        if (currentPosition != DISPLAY_RIGHT) {
            lastPosition = currentPosition;
            currentPosition = DISPLAY_RIGHT;
        }
    } else if (az < 1.10f && az > 0.90f && abs(ax) < 0.20f && abs(ay) < 0.20f) {
        if (currentPosition != DISPLAY_DOWN) {
            lastPosition = currentPosition;
            currentPosition = DISPLAY_DOWN;
        }
    } else if(az > -1.10f && az < -0.90f && abs(ax) < 0.20f && abs(ay) < 0.20f) {
        if (currentPosition != DISPLAY_UP) {
            lastPosition = currentPosition;
            currentPosition = DISPLAY_UP;
            playStandBySound();
        }
        //Save current acceleration values for gesture detection
        baselineX = ax;
        baselineY = ay;
        baselineZ = az;
    } else {
        if (currentPosition != UNKNOWN) {
            lastPosition = currentPosition;
            currentPosition = UNKNOWN;
        }
    }
}

//Function for printing device orientation to screen
Void printPosition(Display_Handle hDisplay) {
    //Clear display if needed
    if (clearDisplay) {
        clearDisplay = 0;
        Display_clear(hDisplay);
    }
    char p[32];
    char k[32];
    char l[32];
    char f[32];
    //Display message on screen if needed
    if (showMessage) {
        sprintf(l, "%u%s", sender, " Transmits:");
        sprintf(f, "%s", message);
    } else {
        sprintf(l, "%s", " ");
        sprintf(f, "%s", " ");
    }
    //Check the current position and print the corresponding string to the screen
    switch (currentPosition) {
        case UNKNOWN:
            sprintf(p, "%s", "Position Unknown");
            sprintf(k, "%s", "Calm Down");
            break;
        case DISPLAY_IN:
            sprintf(p, "%s", "Arm Is Up");
            sprintf(k, "%s", "Do it");
            break;
        case DISPLAY_OUT:
            sprintf(p, "%s", "Arm Is Down");
            sprintf(k, "%s", "Arm Up");
            break;
        case DISPLAY_LEFT:
            sprintf(p, "%s", "Palm Is Right");
            sprintf(k, "%s", "Rotate Right");
            break;
        case DISPLAY_RIGHT:
            sprintf(p, "%s", "Palm Is Left");
            sprintf(k, "%s", "Rotate Left");
            break;
        case DISPLAY_UP:
            sprintf(p, "%s", "Palm Is Down");
            sprintf(k, "%s", "Do it");
            break;
        case DISPLAY_DOWN:
            sprintf(p, "%s", "Palm Is Up");
            sprintf(k, "%s", "Palm Up");
            break;
    }
    //Display texts on screen
    Display_print0(hDisplay, 1, 0, p);
    Display_print0(hDisplay, 9, 0, k);
    Display_print0(hDisplay, 4, 0, l);
    Display_print0(hDisplay, 5, 0, f);
}

//Function for detecting a gestures
Void detectGesture() {
    //Available gestures: High Five, Low Five, Fist Bump and Fist Up(AKA up in the ass of Timo manouever performed by Marcus Grönholm)
    //Gesture detection is made by checking the device orientation and current acceleration
    //Try to prevent cheating by limiting rotation
    if ((fabs(gx) < 90) && (fabs(gy) < 90) && (fabs(gz) < 90)) {
        //Check for correct orientation
        if (lastPosition == DISPLAY_IN && currentPosition == UNKNOWN) {
            //Check for acceleration direction and thresholds to detect the gesture
            if ((fabs(fabs(baselineZ) - fabs(az)) >= 0.5f) 
                && (fabs(fabs(baselineX) - fabs(ax)) <= 0.3f) 
                && (fabs(fabs(baselineY) - fabs(ay)) <= 0.3f)
                && az > baselineZ) {
                //Inform user of their success by sound and text
                playSuccessSound("High Five :b", "Detected");
                gestureH5Count++;
                sendMessage("H5");
            //Check for acceleration direction and thresholds to detect the gesture
            } else if ((fabs(fabs(baselineX) - fabs(ax)) >= 0.5f)
                && (fabs(fabs(baselineY) - fabs(ay)) <= 0.2f)
                && (fabs(fabs(baselineZ) - fabs(az)) <= 0.3f)
                && ax > baselineX) {
                //Inform user of their success by sound and text
                playSuccessSound("Fist Up :b", "Detected");
                gestureFUCount++;
                sendMessage("FU");
            }
        //Check for correct orientation
        } else if (lastPosition == DISPLAY_UP && currentPosition == UNKNOWN) {
            //Check for acceleration direction and thresholds to detect the gesture
            if ((fabs(fabs(baselineZ) - fabs(az)) >= 0.5f) 
                && (fabs(fabs(baselineX) - fabs(ax)) <= 0.3f) 
                && (fabs(fabs(baselineY) - fabs(ay)) <= 0.3f)
                && az > baselineZ) {
                //Inform user of their success by sound and text
                playSuccessSound("Low Five :b", "Detected");
                gestureL5Count++;
                sendMessage("L5");
            //Check for acceleration direction and thresholds to detect the gesture
            } else if ((fabs(fabs(baselineX) - fabs(ax)) >= 0.5f)
                && (fabs(fabs(baselineY) - fabs(ay)) <= 0.3f)
                && (fabs(fabs(baselineZ) - fabs(az)) <= 0.5f)
                && ax > baselineX) {
                //Inform user of their success by sound and text
                playSuccessSound("Fist Bump :b", "Detected");
                gestureFBCount++;
                sendMessage("FB");
            }
        }
    }
}

//Function for printing a received message on screen
Void printMessage() {
    //Clear display if needed
    if (clearDisplay) {
        Display_clear(hDisplay);
        clearDisplay = 0;
    }
    //Display message on screen
    char p[32];
    char k[32];
    sprintf(p, "%u%s", sender, " Transmits:");
    sprintf(k, "%s", message);
    //Print message to screen
    Display_print0(hDisplay, 4, 0, p);
    Display_print0(hDisplay, 5, 0, k);
}

//Function for sending a message
Void sendMessage(char msg[]) {
    char str[8];
    sprintf(str, "%s", msg);
    Send6LoWPAN(0xFFFF, str, 2);
    StartReceive6LoWPAN();
}

//Function for playing the standby sound
Void playStandBySound() {
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(3000);
    Task_sleep(100000 / Clock_tickPeriod);
    buzzerSetFrequency(4500);
    Task_sleep(100000 / Clock_tickPeriod);
    buzzerClose();
}

//Function for playin the success sound and printing detected gesture on screen
Void playSuccessSound(char str1[], char str2[]) {
    //Display detected gesture on screen
    char p[32];
    char k[32];
    sprintf(p, "%s", str1);
    sprintf(k, "%s", str2);
    //Print detected gesture to screen
    Display_print0(hDisplay, 1, 0, p);
    Display_print0(hDisplay, 9, 0, k);
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(200);
    Task_sleep(150000 / Clock_tickPeriod);
    buzzerSetFrequency(400);
    Task_sleep(150000 / Clock_tickPeriod);
    buzzerSetFrequency(600);
    Task_sleep(150000 / Clock_tickPeriod);
    buzzerSetFrequency(900);
    Task_sleep(150000 / Clock_tickPeriod);
    buzzerSetFrequency(600);
    Task_sleep(150000 / Clock_tickPeriod);
    buzzerSetFrequency(1200);
    Task_sleep(250000 / Clock_tickPeriod);
    buzzerSetFrequency(1600);
    Task_sleep(100000 / Clock_tickPeriod);
    buzzerClose();
}

//Function for playing elevator music
Void playElevatorMusic() {
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(300);
    Task_sleep(450000 / Clock_tickPeriod);
    buzzerSetFrequency(400);
    Task_sleep(250000 / Clock_tickPeriod);
    buzzerSetFrequency(550);
    Task_sleep(650000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(100000 / Clock_tickPeriod);
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(350);
    Task_sleep(350000 / Clock_tickPeriod);
    buzzerSetFrequency(400);
    Task_sleep(450000 / Clock_tickPeriod);
    buzzerSetFrequency(450);
    Task_sleep(250000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(100000 / Clock_tickPeriod);
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(300);
    Task_sleep(450000 / Clock_tickPeriod);
    buzzerSetFrequency(400);
    Task_sleep(250000 / Clock_tickPeriod);
    buzzerSetFrequency(550);
    Task_sleep(650000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(100000 / Clock_tickPeriod);
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(350);
    Task_sleep(350000 / Clock_tickPeriod);
    buzzerSetFrequency(400);
    Task_sleep(450000 / Clock_tickPeriod);
    buzzerSetFrequency(450);
    Task_sleep(250000 / Clock_tickPeriod);
    buzzerSetFrequency(300);
    buzzerClose();
}

//Function for playing notification sound
Void playNotificationSound() {
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(6000);
    Task_sleep(50000 / Clock_tickPeriod);
    buzzerClose();
    Task_sleep(50000 / Clock_tickPeriod);
    buzzerOpen(buzzerHandle);
    buzzerSetFrequency(6000);
    Task_sleep(50000 / Clock_tickPeriod);
    buzzerClose();
}

//Main task for device, basically like the main function
Void mainTaskFxn(UArg arg0, UArg arg1) {
    //Set up the display when device is started
    displayParams.lineClearMode = DISPLAY_CLEAR_BOTH;
    Display_Params_init(&displayParams);

    hDisplay = Display_open(Display_Type_LCD, &displayParams);
    if (hDisplay == NULL) {
        System_abort("Error initializing Display\n");
    }
    Display_clear(hDisplay);
    //Infinite loop for Finite-State Machine
    while (1) {
        //Finite-State Machine
        //Check current state
        switch (myState) {
            //Menu screen state
            case MENU:
                if (updateDisplay) {
                    updateDisplay = 0;
                    drawMenu(hDisplay);
                }
                Task_sleep(1000 / Clock_tickPeriod);
                break;
            //Stats screen state
            case STATS:
                //Opposite for stats to prevent screen hogging
                if (!updateDisplay) {
                    updateDisplay = 1;
                    drawStats(hDisplay);
                }
                playElevatorMusic();
                Task_sleep(1000 / Clock_tickPeriod);
                break;
            //Gesture detection state
            case GESTURE:
                //Detect gesture
                updatePosition();
                printPosition(hDisplay);
                detectGesture();
                Task_sleep(10000 / Clock_tickPeriod);
                break;
            //Message displaying state
            case MSG_WAITING:
                if (updateDisplay) {
                    printMessage();
                }
                break;
            //Default state: ANIMATION
            default:
                playAnimation(&hDisplay);
        	    Display_clear(hDisplay);
                Task_sleep(100000 / Clock_tickPeriod);
                break;
        }
    }
}

//Main function
Int main(void) {

    // Task variables
	Task_Handle commTask;
	Task_Params commTaskParams;
	
	Task_Handle mpuTask;
	Task_Params mpuTaskParams;
	
	Task_Handle mainTask;
	Task_Params mainTaskParams;

    // Initialize board
    Board_initGeneral();
    // Initialize i2c
    Board_initI2C();
    
    // Open MPU power pin
    mpuHandle = PIN_open(&mpuState, mpuConfig);
    if (mpuHandle == NULL) {
    	System_abort("MPU power pin open failed!");
    }

    //Open button pin
    buttonHandle = PIN_open(&buttonState, buttonConfig);
    if(!buttonHandle) {
        System_abort("Error initializing button pins\n");
    }
    //Open LED pin
    ledHandle = PIN_open(&ledState, ledConfig);
    if(!ledHandle) {
        System_abort("Error initializing LED pins\n");
    }
    //Registering interrupt handler for button
    if (PIN_registerIntCb(buttonHandle, &buttonFxn) != 0) {
        System_abort("Error registering button callback function");
    }
    
    //Setting up MPU task
    Task_Params_init(&mpuTaskParams);
    mpuTaskParams.stackSize = STACKSIZE;
    mpuTaskParams.stack = &taskStack;
    mpuTaskParams.priority = 2;
    mpuTask = Task_create((Task_FuncPtr)sensorFxn, &mpuTaskParams, NULL);
    if (mpuTask == NULL) {
    	System_abort("Task create failed!");
    }
    
    //Setting up Main task
    Task_Params_init(&mainTaskParams);
    mainTaskParams.stackSize = STACKSIZE;
    mainTaskParams.stack = &mainTaskStack;
    mainTaskParams.priority = 2;
    mainTask = Task_create((Task_FuncPtr)mainTaskFxn, &mainTaskParams, NULL);
    if (mainTask == NULL) {
    	System_abort("Task create failed!");
    }

    //Setting up Communication Task
    Init6LoWPAN(); // This function call before use!
    Task_Params_init(&commTaskParams);
    commTaskParams.stackSize = STACKSIZE;
    commTaskParams.stack = &commTaskStack;
    commTaskParams.priority=1;
    commTask = Task_create(commTaskFxn, &commTaskParams, NULL);
    if (commTask == NULL) {
    	System_abort("Task create failed!");
    }
    
    //Open power button pin
    powerHandle = PIN_open(&powerState, powerFConfig);
    if(!powerHandle) {
        System_abort("Error initializing button shut pins\n");
    }
    
    //Registering interrupt handler for power button
    if (PIN_registerIntCb(powerHandle, &buttonShutFxn) != 0) {
        System_abort("Error registering button callback function");
    }
    
    //Open Buzzer pin
    buzzerHandle = PIN_open(&buzzerState, buzzerConfig);
    if (!buzzerHandle) {
        System_abort("Error initializing buzzer pin\n");
    }
    
    /* Start BIOS */
    BIOS_start();

    return (0);
}

