/*
 *  Made by:
 *  Saku Salo
 *  Iina Lehtihalmes
 */

#ifndef ABC_H_
#define ABC_H_

#include <ti/mw/display/Display.h>
#include <ti/mw/display/DisplayExt.h>

void stopAnimation();

void playAnimation(Display_Handle *hDisplay);

void playAnimation1(Display_Handle *hDisplay);

void playAnimation2(Display_Handle *hDisplay);

void printA(int printIndex, tContext *pContext, int printLine, int x);

void printB(int printIndex, tContext *pContext, int printLine, int x);

void printD(int printIndex, tContext *pContext, int printLine, int x);

void printE(int printIndex, tContext *pContext, int printLine, int x);

void printF(int printIndex, tContext *pContext, int printLine, int x);

void printG(int printIndex, tContext *pContext, int printLine, int x);

void printI(int printIndex, tContext *pContext, int printLine, int x);

void printK(int printIndex, tContext *pContext, int printLine, int x);

void printL(int printIndex, tContext *pContext, int printLine, int x);

void printM(int printIndex, tContext *pContext, int printLine, int x);

void printN(int printIndex, tContext *pContext, int printLine, int x);

void printP(int printIndex, tContext *pContext, int printLine, int x);

void printR(int printIndex, tContext *pContext, int printLine, int x);

void printS(int printIndex, tContext *pContext, int printLine, int x);

void printX(int printIndex, tContext *pContext, int printLine, int x);

void printY(int printIndex, tContext *pContext, int printLine, int x);

#endif
