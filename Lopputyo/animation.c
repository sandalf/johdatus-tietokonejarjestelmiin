/*
 *  Made by:
 *  Saku Salo 
 *  Iina Lehtihalmes
 */

#include <ti/mw/display/Display.h>
#include <ti/mw/display/DisplayExt.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>

//TODO IF TIME: Clean-up, turn if-else hell to arrays

//Loop variable
int i;

//Function for stopping the animation
void stopAnimation() {
    i = 100;
}

//Function for playing the animation
void playAnimation(Display_Handle* hDisplay) {
    Display_clear(*hDisplay);
    tContext *pContext = DisplayExt_getGrlibContext(*hDisplay);
    if (pContext) {
        playAnimation1(pContext);
        Task_sleep(1000000 / Clock_tickPeriod);
        Display_clear(*hDisplay);
        //Check if animation is stopped
        if (i <= 97) {
            playAnimation2(pContext);
            Task_sleep(1000000 / Clock_tickPeriod);
        }
    }
    Display_clear(*hDisplay);
}

//Function for animation 1
//Very ugly and lazy test code, please don't judge
void playAnimation1(tContext *pContext) {
    GrLineDrawH(pContext, 0, 96, 0);
    GrFlush(pContext);
    Task_sleep(100000 / Clock_tickPeriod);
    for (i = 1; i <= 96; i++) {
        if ((i >= 6 && i <= 13) || (i >= 48 && i <= 55)) {
            GrLineDrawH(pContext, 0, 19, i);
            GrLineDrawH(pContext, 76, 96, i);
            //For outlines only
            if (i == 13 || i == 48 || i == 55) {
                GrLineDrawH(pContext, 45, 50, i);
            } else if (i != 6) {
                GrLineDrawH(pContext, 21, 74, i);
            }
        } else if ((i >= 14 && i <= 47) || (i >= 56 && i <= 89 )) {
            if (i < 15) {
                GrLineDrawH(pContext, 25, 25, i);
            }
            if (i < 16) {
                GrLineDrawH(pContext, 27, 27, i);
            }
            if (i < 18) {
                GrLineDrawH(pContext, 29, 29, i);
            }
            if (i < 20) {
                GrLineDrawH(pContext, 31, 31, i);
            }
            if (i <21) {
                GrLineDrawH(pContext, 33, 33, i);
            }
            if (i < 24) {
                GrLineDrawH(pContext, 35, 35, i);
            }
            if (i < 27) {
                GrLineDrawH(pContext, 37, 37, i);
            }
            if (i < 32) {
                GrLineDrawH(pContext, 39, 39, i); 
            }
            if (i < 40) {
                GrLineDrawH(pContext, 41, 41, i); 
            }
            if (i < 46) { //47
                GrLineDrawH(pContext, 43, 43, i);
            }
            if (i == 14) {
                GrLineDrawH(pContext, 0, 21, i);
            } else if (i == 15) {
                GrLineDrawH(pContext, 0, 23, i);
            } else if (i == 16) {
                GrLineDrawH(pContext, 0, 25, i);
            } else if (i <= 18) {
                GrLineDrawH(pContext, 0, 27, i);
            } else if (i <= 20) {
                GrLineDrawH(pContext, 0, 29, i);
            } else if (i == 21) {
                GrLineDrawH(pContext, 0, 31, i);
            } else if (i <= 24) {
                GrLineDrawH(pContext, 0, 33, i);
            } else if (i <= 27) {
                GrLineDrawH(pContext, 0, 35, i);
            } else if (i <= 32) {
                GrLineDrawH(pContext, 0, 37, i);
            } else if (i <= 40) {
                GrLineDrawH(pContext, 0, 39, i);
            } else if (i <= 46) { //47
                GrLineDrawH(pContext, 0, 41, i);
                GrLineDrawH(pContext, 43, 43, i);
            } else if (i == 47) {
                GrLineDrawH(pContext, 0, 42, i);
            } else {
                GrLineDrawH(pContext, 0, 43, i);
            }
            if (i <= 47) {
                GrLineDrawH(pContext, 52, 96, i);
            }
            if (i < 57) {
                GrLineDrawH(pContext, 70, 70, i);
            }
            if (i < 58) {
                GrLineDrawH(pContext, 68, 68, i);
            }
            if (i < 60) {
                GrLineDrawH(pContext, 66, 66, i);
            }
            if (i < 62) {
                GrLineDrawH(pContext, 64, 64, i);
            }
            if (i < 63) {
                GrLineDrawH(pContext, 62, 62, i);
            }
            if (i < 66) {
                GrLineDrawH(pContext, 60, 60, i);
            }
            if (i < 69) {
                GrLineDrawH(pContext, 58, 58, i);
            }
            if (i < 74) {
                GrLineDrawH(pContext, 56, 56, i); 
            }
            if (i < 82) {
                GrLineDrawH(pContext, 54, 54, i); 
            }
            if (i < 88) { //89
                GrLineDrawH(pContext, 52, 52, i);
            }
            if (i == 56) {
                GrLineDrawH(pContext, 74, 96, i);
            } else if (i == 57) {
                GrLineDrawH(pContext, 71, 96, i);
            } else if (i == 58) {
                GrLineDrawH(pContext, 70, 96, i);
            } else if (i <= 60) {
                GrLineDrawH(pContext, 68, 96, i);
            } else if (i <= 62) {
                GrLineDrawH(pContext, 66, 96, i);
            } else if (i == 63) {
                GrLineDrawH(pContext, 64, 96, i);
            } else if (i <= 66) {
                GrLineDrawH(pContext, 62, 96, i);
            } else if (i <= 69) {
                GrLineDrawH(pContext, 60, 96, i);
            } else if (i <= 74) {
                GrLineDrawH(pContext, 58, 96, i);
            } else if (i <= 82) {
                GrLineDrawH(pContext, 56, 96, i);
            } else if (i <= 88) { //89
                GrLineDrawH(pContext, 54, 96, i);
                GrLineDrawH(pContext, 52, 52, i);
            } else if (i == 89) {
                GrLineDrawH(pContext, 53, 96, i);
            } else {
                GrLineDrawH(pContext, 52, 96, i);
            }
            if (i != 89) {
                GrLineDrawH(pContext, 45, 50, i);
            }
        } else {
            GrLineDrawH(pContext, 0, 96, i);
        }
        GrFlush(pContext);
        Task_sleep(100000 / Clock_tickPeriod);
    }
}

//Function for animation 2
//Very ugly and lazy test code, please don't judge
void playAnimation2(tContext *pContext) {
    GrLineDrawV(pContext, 0, 0, 96);
    GrFlush(pContext);
    Task_sleep(100000 / Clock_tickPeriod);
    for (i = 1; i <= 96; i++) {
        if (i >= 29 && i <= 33) {
            printE(i - 29, pContext, 22, i);
        } else if (i >= 36 && i <= 40) {
            printL(i - 36, pContext, 22, i);
        } else if (i >= 43 && i <= 47) {
            printE(i - 43, pContext, 22, i);
        } else if (i >= 50 && i <= 54) {
            printN(i - 50, pContext, 22, i);
        } else if (i >= 57 && i <= 61) {
            printA(i - 57, pContext, 22, i);
        } else if (i >= 64 && i <= 68) {
            printI(i - 64, pContext, 22, i);
        } else {
            GrLineDrawV(pContext, i, 22, 28);
        }
        
        if (i >= 35 && i <= 39) {
            printM(i - 35, pContext, 31, i);
        } else if (i >= 42 && i <= 46) {
            printA(i - 42, pContext, 31, i);
        } else if (i >= 49 && i <= 53) {
            printD(i - 49, pContext, 31, i);
        } else if (i >= 56 && i <= 60) {
            printE(i - 56, pContext, 31, i);
        } else {
            GrLineDrawV(pContext, i, 31, 37);
        }
        
        if (i >= 42 && i <= 46) {
            printB(i - 42, pContext, 40, i);
        } else if (i >= 49 && i <= 53) {
            printY(i - 49, pContext, 40, i);
        } else {
            GrLineDrawV(pContext, i, 40 ,46);
        }
        
        if (i >= 24 && i <= 28) {
            printS(i - 24, pContext, 49, i);
        } else if (i >= 31 && i <= 35) {
            printA(i - 31, pContext, 49, i);
        } else if (i >= 38 && i <= 42) {
            printN(i - 38, pContext, 49, i);
        } else if (i >= 45 && i <= 49) {
            printD(i - 45, pContext, 49, i);
        } else if (i >= 52 && i <= 56) {
            printA(i - 52, pContext, 49, i);
        } else if (i >= 59 && i <= 63) {
            printL(i - 59, pContext, 49, i);
        } else if (i >= 66 && i <= 70) {
            printF(i - 66, pContext, 49, i);
        } else {
            GrLineDrawV(pContext, i, 49, 55);
        }
        
        if (i >= 21 && i <= 25) {
            printI(i - 21, pContext, 58, i);
        } else if (i >= 28 && i <= 32) {
            printP(i - 28, pContext, 58, i);
        } else if (i >= 35 && i <= 39) {
            printP(i - 35, pContext, 58, i);
        } else if (i >= 42 && i <= 46) {
            printE(i - 42, pContext, 58, i);
        } else if (i >= 49 && i <= 53) {
            printL(i - 49, pContext, 58, i);
        } else if (i >= 56 && i <= 60) {
            printI(i - 56, pContext, 58, i);
        } else if (i >= 63 && i <= 67) {
            printX(i - 63, pContext, 58, i);
        } else if (i >= 70 && i <= 74) {
            printD(i - 70, pContext, 58, i);
        } else {
            GrLineDrawV(pContext, i, 58, 64);
        }
        
        if (i >= 21 && i <= 25) {
            printA(i - 21, pContext, 67, i);
        } else if (i >= 28 && i <= 32) {
            printG(i - 28, pContext, 67, i);
        } else if (i >= 35 && i <= 39) {
            printR(i - 35, pContext, 67, i);
        } else if (i >= 42 && i <= 46) {
            printI(i - 42, pContext, 67, i);
        } else if (i >= 49 && i <= 53) {
            printM(i - 49, pContext, 67, i);
        } else if (i >= 56 && i <= 60) {
            printA(i - 56, pContext, 67, i);
        } else if (i >= 63 && i <= 67) {
            printR(i - 63, pContext, 67, i);
        } else if (i >= 70 && i <= 74) {
            printK(i - 70, pContext, 67, i);
        } else {
            GrLineDrawV(pContext, i, 67, 73);
        }
        
        GrLineDrawV(pContext, i, 0, 21);
        GrLineDrawV(pContext, i, 29, 30);
        GrLineDrawV(pContext, i, 38, 39);
        GrLineDrawV(pContext, i, 47, 48);
        GrLineDrawV(pContext, i, 56, 57);
        GrLineDrawV(pContext, i, 65, 66);
        GrLineDrawV(pContext, i, 74, 96);
        GrFlush(pContext);
        Task_sleep(100000 / Clock_tickPeriod);
    }
}

//Function for printing A
void printA(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
        case 4:
            GrLineDrawV(pContext, x, printLine, printLine);
            break;
        case 1:
        case 2:
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 6);
            break;
    }
}

//Function for printing B
void printB(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
            break;
        case 1:
        case 2:
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 5);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 3, printLine + 3);
            GrLineDrawV(pContext, x, printLine + 6, printLine + 6);
            break;
    }
}

//Function for printing D
void printD(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
            break;
        case 1:
        case 2:
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 5);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 6, printLine + 6);
            break;
    }
}

//Function for printing E
void printE(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
            break;
        case 1:
        case 2:
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 5);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 5);
            break;
    }
}

//Function for printing F
void printF(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
            break;
        case 1:
        case 2:
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 6);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 6);
            break;
    }
}

//Function for printing G
void printG(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 6, printLine + 6);
            break;
        case 1:
        case 2:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 5);
            break;
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 3);
            GrLineDrawV(pContext, x, printLine + 5, printLine + 5);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 3);
            GrLineDrawV(pContext, x, printLine + 6, printLine + 6);
            break;
    }
}

//Function for printing I
void printI(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
        case 1:
        case 3:
        case 4:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 5);
            break;
    }
}

//Function for printing K
void printK(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 1:
            GrLineDrawV(pContext, x, printLine, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 6);
            break;
        case 2:
            GrLineDrawV(pContext, x, printLine, printLine + 1);
            GrLineDrawV(pContext, x, printLine + 3, printLine + 3);
            GrLineDrawV(pContext, x, printLine + 5, printLine + 6);
            break;
        case 3:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 2, printLine + 4);
            GrLineDrawV(pContext, x, printLine + 6, printLine + 6);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 5);
            break;
    }
}

//Function for printing L
void printL(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 1:
        case 2:
        case 3:
        case 4:
            GrLineDrawV(pContext, x, printLine, printLine + 5);
            break;
    }
}

//Function for printing M
void printM(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 1:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 2, printLine + 6);
            break;
        case 2:
            GrLineDrawV(pContext, x, printLine, printLine + 1);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 6);
            break;
        case 3:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 2, printLine + 6);
            break;
    }
}

//Function for printing N
void printN(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 1:
            GrLineDrawV(pContext, x, printLine, printLine + 1);
            GrLineDrawV(pContext, x, printLine + 3, printLine + 6);
            break;
        case 2:
            GrLineDrawV(pContext, x, printLine, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 6);
            break;
        case 3:
            GrLineDrawV(pContext, x, printLine, printLine + 3);
            GrLineDrawV(pContext, x, printLine + 5, printLine + 6);
            break;
    }
}

//Function for printing P
void printP(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 1:
        case 2:
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 6);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 3, printLine + 6);
            break;
    }
}

//Function for printing R
void printR(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 1:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 6);
            break;
        case 2:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 5, printLine + 6);
            break;
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 3);
            GrLineDrawV(pContext, x, printLine + 6, printLine + 6);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 3, printLine + 5);
            break;
    }
}

//Function for printing S
void printS(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
            GrLineDrawV(pContext, x, printLine, printLine);
            GrLineDrawV(pContext, x, printLine + 3, printLine + 5);
            break;
        case 1:
        case 2:
        case 3:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 5);
            break;
        case 4:
            GrLineDrawV(pContext, x, printLine + 1, printLine + 3);
            GrLineDrawV(pContext, x, printLine + 6, printLine + 6);
            break;
    }
}

//Function for printing X
void printX(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
        case 4:
            GrLineDrawV(pContext, x, printLine + 2, printLine + 4);
            break;
        case 1:
        case 3:
            GrLineDrawV(pContext, x, printLine, printLine + 1);
            GrLineDrawV(pContext, x, printLine + 5, printLine + 6);
            break;
        case 2:
            GrLineDrawV(pContext, x, printLine, printLine + 2);
            GrLineDrawV(pContext, x, printLine + 4, printLine + 6);
            break;
    }
}

//Function for printing Y
void printY(int printIndex, tContext *pContext, int printLine, int x) {
    switch(printIndex) {
        case 0:
        case 4:
            GrLineDrawV(pContext, x, printLine + 2, printLine + 6);
            break;
        case 1:
        case 3:
            GrLineDrawV(pContext, x, printLine, printLine + 1);
            GrLineDrawV(pContext, x, printLine + 3, printLine + 6);
            break;
        case 2:
            GrLineDrawV(pContext, x, printLine + 0, printLine + 2);
            break;
    }
}