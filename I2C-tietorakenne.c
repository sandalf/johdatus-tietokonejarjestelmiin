#include <inttypes.h>
#include <stdio.h>

struct i2c_message {
    uint8_t sensorRegister;
    uint8_t slaveAddress;
    char *writeBuf;
    uint8_t writeCount;
    char *readBuf;
    uint8_t readCount;
};

void print_i2c(struct i2c_message *msg);

void print_i2c(struct i2c_message *msg) {
	printf("sensorRegister:%02x\n", msg->sensorRegister);
	printf("slaveAddress:%02x\n", msg->slaveAddress);
	printf("writeBuf:");
	int i;
	for (i = 0; i < msg->writeCount; i++) {
		printf("%x", msg->writeBuf[i]);
	}
	printf("\nreadBuf:");
	for (i = 0; i < msg->readCount; i++) {
		printf("%x", msg->readBuf[i]);
	}
	printf("\n");
}